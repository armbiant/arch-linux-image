## FLYNNt OS
This is a lightweight, low footprint, Arch-based Linux distribution with slightly more secure defaults.
It is primarily meant for use as a livedisk or in a virtual machine.


![screenshot](screenshot.jpg)


### Download
- Coming soon...


### Run

Insert / attach the ISO to your virtual machine like any other Linux ISO.

#### If you're using QEMU/KVM with an Nvidia graphics card:

Replace the default video XML in you virtual machine's config with:

```xml
<video>
  <model type="qxl" ram="131072" vram="131072" vgamem="131072" heads="1" primary="yes"/>
  <address type="pci" domain="0x0000" bus="0x00" slot="0x01" function="0x0"/>
</video>
```

### Configure
- Packages to be installed will be in `packages.x86_64`
- Boot parameters are found in `./grub/grub.cfg` (for GRUB), and `./syslinux/archiso_sys-linux.cfg` (for Syslinux)
- Kernel sysctl config is found in `./airootfs/etc/99-sysctl.conf`
- Kernel module blacklist is found in `./airootfs/etc/modprobe.d/blacklist.conf`


### Build Requirements
- ISO files can be built on Arch or any Arch-based Linux distribution.
- `mkarchiso` needs to be installed (install with `sudo pacman -S archiso`).


### Build Instructions
0. Clone the repo with `git clone https://gitlab.com/rlyx/flynnt.git`
1. Enter the directory with `cd flynnt`
2. Add any packages you want into `packages.x86_64`
3. Make any configuration changes you want
4. Build ISO with `./build.sh`

The generated ISO file will be found in the `../BUILD` folder (above the repo folder).