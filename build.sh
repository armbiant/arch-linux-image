#!/usr/bin/env bash

# check if archiso is installed
if [ ! $(which mkarchiso 2> /dev/null) ]; then
  echo "ERROR: mkarchiso command not found";
  echo "Install with: sudo pacman -S archiso";
  exit 1;
fi;

BUILD_DIRECTORY="BUILD";

# check if build directory exists
if [ -d ../"$BUILD_DIRECTORY" ]; then
  echo "Found build directory: ../$BUILD_DIRECTORY";
else
  echo "Making build directory: ../$BUILD_DIRECTORY";
  mkdir ../"$BUILD_DIRECTORY";
fi;

# make the actual arch iso
sudo mkarchiso -v -w ../"$BUILD_DIRECTORY" -o ../"$BUILD_DIRECTORY" . ;
sleep 2;

# remove build artefacts
cd ../"$BUILD_DIRECTORY";
shopt -s extglob;
if [ -f ]; then
  echo "Removing intermediate build files...";
  sudo rm -r {grub,iso,x86_64,arm};
  sudo rm !(*.iso);
fi

exit 0;
