#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="FLYNNt OS"
iso_label="ARCH_$(date +%Y%m)"
#iso_publisher="Arch Linux <https://archlinux.org>"
iso_publisher="rlyx <https://gitlab.com/rlyx/flynnt> (Based on Arch Linux <https://archlinux.org>)"
iso_application="FLYNNt (Arch Linux) Live Disk"
iso_version="$(date +%Y.%m.%d)"
install_dir="arch"
buildmodes=('iso')

# default / recommended
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.grub.esp' 'uefi-x64.grub.eltorito')

# the boot mode settings below are NOT recommended, only use this if you know what you're doing
#bootmodes=('bios.syslinux.eltorito' 'uefi-x64.grub.esp' 'uefi-x64.grub.eltorito')

arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M')
file_permissions=(
  ["/boot"]="0:0:700"
  ["/etc/shadow"]="0:0:0400"
  ["/etc/gshadow"]="0:0:0400"
  ["/etc/sudoers"]="0:0:0440"
  ["/home"]="0:0:750"
  ["/root"]="0:0:700"
  ["/srv"]="0:0:700"
  ["/usr/lib/modules"]="0:0:700"
  ["/usr/local/bin/choose-mirror"]="0:0:755"
  ["/usr/local/bin/livecd-sound"]="0:0:755"
  ["/usr/src"]="0:0:700"
)
