#
# ~/.bash_profile
#

#xdg-user-dirs-update

[[ -f ~/.bashrc ]] && . ~/.bashrc

export TERM=foot

# sway wm:

# fix for cursor not showing up in vm:
export WLR_NO_HARDWARE_CURSORS=1

# run firefox in wayland by default:
export MOZ_ENABLE_WAYLAND=1

# fix for java applications:
export _JAVA_AWT_WM_NONREPARENTING=1

# misc. fixes:
export GDK_BACKEND=wayland
export CLUTTER_BACKEND=wayland

# qt specific fixes:
export QT_QPA_PLATFORM=wayland
export QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1

export WLR_DRM_NO_MODIFIERS=1
#export WLR_RENDERER_ALLOW_SOFTWARE=1

# force the use of GBM as backend for nvidia:
#export GBM_BACKEND=nvidia-drm
#export __GLX_VENDOR_LIBRARY_NAME=nvidia

export __EGL_VENDOR_LIBRARY_FILENAMES=/usr/share/glvnd/egl_vendor.d/50_mesa.json

if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then

  # select the first gpu:
  #gpu=$(udevadm info -a -n /dev/dri/card1 | grep boot_vga | rev | cut -c 2)
  #export WLR_DRM_DEVICES="/dev/dri/card$gpu"
  export WLR_DRM_DEVICES="/dev/dri/card0"
  
  # for running on proprietary drivers:
  #exec sway --unsupported-gpu > ~/.sway.log 2>&1
  export XDG_CURRENT_DESKTOP=sway
  export XDG_SESSION_TYPE=wayland
  exec sway > ~/.sway.log 2>&1
fi