# check currently loaded kernel modules with:
# kmod list;

## BLUETOOTH ------------------------------------------------------------------
# comment these lines out if you use bluetooth
install bluetooth /bin/false
install btusb /bin/false
install bnep /bin/false


## FILESYSTEMS ----------------------------------------------------------------
# replaced by squashfs:
install cramfs /bin/false

# apple specific legacy fs:
install hfs /bin/false
install hfsplus /bin/false
install mac_celtic /bin/false
install mac_centeuro /bin/false
install mac_croatian /bin/false
install mac_cyrillic /bin/false
install mac_gaelic /bin/false
install mac_greek /bin/false
install mac_iceland /bin/false
install mac_inuit /bin/false
install mac_romanian /bin/false
install mac_roman /bin/false
install mac_turkish /bin/false

# huawei enhanced rom fs:
install erofs /bin/false

# uncomment the line below if you don't use optical media (cds, etc.)
#install udf /bin/false

# nilfs:
install nilfs2 /bin/false

# oracle cluster fs:
install ocfs2 /bin/false
install ocfs2_stackglue /bin/false
install ocfs2_stack_o2cb /bin/false
install ocfs2_stack_user /bin/false
install ocfs2_nodemanager /bin/false
install ocfs2_dlm /bin/false
install ocfs2_dlmfs /bin/false

# overlay fs:
#install overlay /bin/false

# virtualbox module for host fs access:
install vboxsf /bin/false

# misc / old fs:
install 9p /bin/false
install affs /bin/false
install befs /bin/false
install freevxfs /bin/false
install jffs2 /bin/false
install kafs /bin/false
install minix /bin/false
install omfs /bin/false
install reiserfs /bin/false
install romfs /bin/false
install xfs /bin/false

# various network / distributed filesystems:
install ceph /bin/false
install coda /bin/false
install gfs2 /bin/false
install ksmbd /bin/false
install nfsd /bin/false
install nfsv2 /bin/false
install nfsv3 /bin/false
install orangefs /bin/false

## FIREWIRE -------------------------------------------------------------------
install firewire_core /bin/false
install firewire_ohci /bin/false
install firewire_sbp2 /bin/false
install ohci1394 /bin/false
install sbp2 /bin/false
install dv1394 /bin/false
install raw1394 /bin/false
install video1394 /bin/false
install snd_firewire_lib /bin/false


## FLOPPY DRIVE ---------------------------------------------------------------
install floppy /bin/false


## INTEL MANAGEMENT ENGINE ----------------------------------------------------
install mei /bin/false
install mei_me /bin/false


## NETWORK PROTOCOLS ----------------------------------------------------------
# remote desktop / management stuff:
install rds /bin/false
install rds_rdma /bin/false
install rds_tcp /bin/false
install rose /bin/false
install rxrpc /bin/false

# for use with clusters:
install tipc /bin/false

# plan 9:
install 9pnet_fd /bin/false
install 9pnet /bin/false
install 9pnet_rdma /bin/false
install 9pnet_virtio /bin/false
install 9pnet_xen /bin/false

# can:
install can /bin/false
install can_j1939 /bin/false
install can_bcm /bin/false
install can_gw /bin/false
install can_isotp /bin/false
install can_raw /bin/false

# very outdated protocols:
install x25 /bin/false
install decnet /bin/false
install econet /bin/false
install ipx /bin/false
install appletalk /bin/false

# virtualization:
install hv_sock /bin/false
install vsock /bin/false
install vsock_diag /bin/false
install vsock_loopback /bin/false

# uncommon protocols:
install n_hdlc /bin/false
install ax25 /bin/false
install netrom /bin/false
install af_802154 /bin/false
install psnap /bin/false
install p8023 /bin/false
#install llc /bin/false
install dccp /bin/false
#install sctp /bin/false
install p8022 /bin/false
install libceph /bin/false


## SCSI -----------------------------------------------------------------------
install libiscsi /bin/false
install libiscsi_tcp /bin/false
install ipr /bin/false
install hpsa /bin/false
install scsi_transport_fc /bin/false
install scsi_transport_iscsi /bin/false
install scsi_transport_sas /bin/false
install scsi_transport_spi /bin/false
install scsi_debug /bin/false
install iscsi_tcp /bin/false
install pmcraid /bin/false
install BusLogic /bin/false
install advansys /bin/false
install st /bin/false
install ips /bin/false
install myrs /bin/false
install myrb /bin/false
install megaraid /bin/false
install sg /bin/false
install ses /bin/false
install dc395x /bin/false
install vmw_pvscsi /bin/false


## SOUND ----------------------------------------------------------------------
#firewire:
install snd_bebob /bin/false
install snd_dice /bin/false
install snd_firewire_digi00x /bin/false
install snd_fireface /bin/false
install snd_fireworks /bin/false
install snd_firewire_motu /bin/false
install snd_oxfw /bin/false
install snd_oxfw /bin/false
install snd_firewire_tascam /bin/false

#misc:
install snd_ali5451 /bin/false
install snd_au8810 /bin/false
install snd_au8820 /bin/false
install snd_au8830 /bin/false
install snd_aw2 /bin/false
install snd_cs46xx /bin/false
install snd_darla20 /bin/false
install snd_darla24 /bin/false
install snd_echo3g /bin/false
install snd_emu10k1 /bin/false
install snd_emu10k1-synth /bin/false
install snd_emu10k1x /bin/false
install snd_gina20 /bin/false
install snd_gina24 /bin/false
install snd_indigodj /bin/false
install snd_indigodjx /bin/false
install snd_indigoio /bin/false
install snd_indigoiox /bin/false
install snd_indigo /bin/false
install snd_korg1212 /bin/false
install snd_layla20 /bin/false
install snd_layla24 /bin/false
install snd_lola /bin/false
install snd_lx6464es /bin/false
install snd_mia /bin/false
install snd_mixart /bin/false
install snd_mona /bin/false
install snd_pdaudiocf /bin/false
install snd_rawmidi /bin/false
install snd_riptide /bin/false
install snd_sb_common /bin/false
install snd_serial_u16550 /bin/false
install snd_trident /bin/false
install snd_vx_lib /bin/false
install snd_vx222 /bin/false
install snd_vxpocket /bin/false
install snd_ymfpci /bin/false


## THUNDERBOLT ----------------------------------------------------------------
install thunderbolt /bin/false


## VIDEO / GPU ----------------------------------------------------------------
# useless test driver:
install vivid /bin/false

# virtual gpus:
install hyperv_drm /bin/false
install vboxvideo /bin/false
install vmwgfx /bin/false

# misc:
install cirrus /bin/false
install gma500_gfx /bin/false


## VIRTUALIZATION -------------------------------------------------------------
install hv_balloon /bin/false
install hv_utils /bin/false
install hv_vmbus /bin/false


## WEBCAM ---------------------------------------------------------------------
# remove the line below if you actually use a webcam
install uvcvideo /bin/false

install stkwebcam /bin/false


## WMI ------------------------------------------------------------------------
install wmi /bin/false
install intel_wmi_thunderbolt /bin/false


## MISCELLANEOUS --------------------------------------------------------------
# prevent loading of miscellaneous kernel modules
install analog /bin/false
install db9 /bin/false
install gamecon /bin/false
install gameport /bin/false
install garmin_gps /bin/false
install hwpoison_inject /bin/false
install parport /bin/false
#blacklist pcspkr

