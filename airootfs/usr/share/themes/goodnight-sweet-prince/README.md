
## ~~Sweet~~ goodnight sweet prince
- This is an edit of the orginal [Sweet](https://github.com/EliverLara/Sweet) theme by [Eliver Lara](https://github.com/EliverLara) to make it much, *much* darker.

![screenshot](screenshot.png)

### Warnings
- This is very much a work in progress, expect visual bugs in ~~certain edge~~ many cases.
- the cinnamon and gnome-shell bits have *NOT* been touched at all (I will eventually get to this).


### Installation (from the original README)

Extract the zip file to the themes directory i.e. `/usr/share/themes/` or `~/.themes/` (create it if necessary).

To set the theme in Gnome, run the following commands in Terminal,

```
gsettings set org.gnome.desktop.interface gtk-theme "goodnight-sweet-prince"
gsettings set org.gnome.desktop.wm.preferences theme "goodnight-sweet-prince"
```
or Change via distribution specific tool.

### Credits
Original repo: https://github.com/EliverLara/Sweet
